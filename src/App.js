import React from 'react';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayValue: ''
    }
  }

  updateDisplay = (text) => {
    this.setState({displayValue: text});
  }
  render = () => {
    return (
        <div id="drum-machine"className="container-fluid bg-light">
          <label id="display" className="form-control">{this.state.displayValue}</label>
          <KeyPad keyDefs={this.props.keyDefs} padKeyClicked={this.updateDisplay}/>
        </div>
    );
  }
}

class KeyPad extends React.Component {
  constructor(props) {
    super(props);
  }
  render = () => {
    return (<div id="pad-keys">
      {this.props.keyDefs && this.props.keyDefs.map(item => <Key keyInfo={item} onClick={this.props.padKeyClicked} />)}
    </div>)
  }
}

class Key extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log('add listener')
    document.addEventListener('keypress', this.keyPressed)
  }

  componentWillUnmount() {
    console.log('remove listener')
    document.removeEventListener('keypress', this.keyPressed)
  }
  keyPressed = (event) => {
    console.log(event)
    if(event.keyCode === this.props.keyInfo.keyCode) {
      this.play(this.props.keyInfo.keyTrigger, this.props.keyInfo.id);
    }
  }
  handleClick = (event) => {
    this.play(event.target.innerText, event.target.id);
  }

  play = (text, id) => {
    const audio = document.getElementById(text);
    this.props.onClick(id);
    audio.play();
  }

  render = () => {
    return (
        <button id={this.props.keyInfo.id} className="btn btn-info btn-lg drum-pad" onClick={this.handleClick}>
          {this.props.keyInfo.keyTrigger}
          <audio id={this.props.keyInfo.keyTrigger} className="clip" src={this.props.keyInfo.url}>
          </audio>
        </button>)
  }
}

export default App;
